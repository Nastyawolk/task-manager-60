package ru.t1.volkova.tm.service;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.IReceiverService;

import javax.jms.*;

@Service
@NoArgsConstructor
public class ReceiverService implements IReceiverService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener entityListener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(entityListener);
    }

}

package ru.t1.volkova.tm.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.EmailEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.LoginEmptyException;
import ru.t1.volkova.tm.exception.field.PasswordEmptyException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final List<UserDTO> userList = new ArrayList<>();

    @NotNull
    private static IProjectDTOService projectService;

    @NotNull
    private static ITaskDTOService taskService;

    @NotNull
    private static IUserDTOService userService;

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        projectService = context.getBean(IProjectDTOService.class);
        taskService = context.getBean(ITaskDTOService.class);
        userService = context.getBean(IUserDTOService.class);

        @NotNull final UserDTO user = userService.create("user", "user", "user@mail.ru");
        userList.add(userService.updateUser(user.getId(), "UserFirstName",
                "UserLastName", "UserMiddleNaeme"));
        @NotNull final UserDTO test = userService.create("test", "test", "test@mail.ru");
        userList.add(userService.updateUser(test.getId(), "TestFirstName",
                "TestLastName", "TestMiddleNaeme"));
    }

    @Test
    public void testCreate(
    ) {
        int size = userService.getRepository().getSize();
        @Nullable final UserDTO user = userService.create("new_login", "password332");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(size + 1, userService.getRepository().getSize());
    }

    @Test
    public void testCreateWithEmail(
    ) {
        int size = userService.getRepository().getSize();
        @Nullable final UserDTO user = userService.create("new_login2", "password332", "new_user@mail.ru");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(size + 1, userService.getRepository().getSize());
    }

    @Test
    public void testCreateWithRole(
    ) {
        int size = userService.getRepository().getSize();
        @Nullable final UserDTO user = userService.create("usual3", "password332", Role.USUAL);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(size + 1, userService.getRepository().getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateLoginEmpty(
    ) {
        userService.create(null, "password332");
        userService.create("", "password332");
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateLoginExists(
    ) {
        userService.create("test", "test");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreatePasswordEmpty(
    ) {
        userService.create("new_user", null);
        userService.create("new_user", "");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmailEmpty(
    ) {
        userService.create("new_login4", "password332", "");
        userService.create("new_login4", "password332", (String) null);
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateRoleEmpty(
    ) {
        userService.create("new_login4", "password332", (Role) null);
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(userList.get(0), userService.findByLogin(userList.get(0).getLogin()));
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertNull(userService.findByLogin("non-existent"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLoginEmpty() {
        Assert.assertEquals(userList.get(1), userService.findByLogin(""));
        Assert.assertEquals(userList.get(0), userService.findByLogin(null));
    }

    @Test
    public void testFindByEmail() {
        Assert.assertEquals(userList.get(0), userService.findByEmail(userList.get(0).getEmail()));
    }

    @Test
    public void testFindByEmailNegative() {
        Assert.assertNull(userService.findByEmail("non-existent"));
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmailEmpty() {
        Assert.assertEquals(userList.get(1), userService.findByEmail(""));
        Assert.assertEquals(userList.get(0), userService.findByEmail(null));
    }

    @Test
    public void testRemoveOne() {
        @Nullable final UserDTO user = userService.findByLogin("test");
        Assert.assertEquals(user, userService.removeOne(user));
        userService.create(user.getLogin(), user.getLogin(), user.getLogin());
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveOneNegative() {
        userService.removeOne(null);
    }

    @Test
    public void testRemoveByLogin() {
        int size = userService.getRepository().getSize();
        @Nullable final UserDTO user = userService.findByLogin("test");
        Assert.assertEquals(user, userService.removeByLogin(user.getLogin()));
        Assert.assertEquals(size - 1, userService.getRepository().getSize());
        userService.create(user.getLogin(), user.getLogin(), user.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByLoginEmptyLogin() {
        userService.removeByLogin(null);
        userService.removeByLogin("");
    }

    @Test
    public void testRemoveByEmail() {
        int size = userService.getRepository().getSize();
        @Nullable final UserDTO user = userService.findByLogin("test");
        Assert.assertEquals(user, userService.removeByEmail(user.getEmail()));
        Assert.assertEquals(size - 1, userService.getRepository().getSize());
        userService.create(user.getLogin(), user.getLogin(), user.getLogin());
    }

    @Test(expected = EmailEmptyException.class)
    public void testRemoveByLoginEmptyEmail() {
        userService.removeByEmail(null);
        userService.removeByEmail("");
    }

    @Test
    public void testSetPassword() {
        Assert.assertNotNull(userService.setPassword(userList.get(1).getId(), "newPass2"));
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordEmptyId() {
        userService.setPassword(null, "newPass");
        userService.setPassword("", "newPass");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPasswordEmptyPassword() {
        userService.setPassword(userList.get(1).getId(), "");
        userService.setPassword(userList.get(1).getId(), null);
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        @NotNull final String id = userService.findByLogin("test").getId();
        @NotNull final UserDTO user = userService.updateUser(id, firstName, lastName, middleName);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        userService.updateUser("non-existent", firstName, lastName, middleName);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        userService.updateUser("", "firstName", "lastName", "middleName");
        userService.updateUser(null, "firstName", "lastName", "middleName");
    }

    @Test
    public void testIsLoginExist() {
        Assert.assertTrue(userService.isLoginExist("user"));
    }

    @Test
    public void testIsLoginExistNegative() {
        Assert.assertFalse(userService.isLoginExist("user4444"));
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void testIsEmailExist() {
        Assert.assertTrue(userService.isEmailExist(userList.get(0).getEmail()));
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final UserDTO user = userService.lockUserByLogin(userList.get(1).getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByEmptyLogin() {
        userService.lockUserByLogin("");
        userService.lockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByEmptyId() {
        userService.lockUserByLogin("non-existent");
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final UserDTO user = userService.unlockUserByLogin(userList.get(1).getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByEmptyLogin() {
        userService.unlockUserByLogin("");
        userService.unlockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnlockUserByEmptyId() {
        userService.unlockUserByLogin("non-existent");
    }

    @Test
    public void testFindOneById() {
        Assert.assertNotNull(userService.findOneById(userList.get(0).getId()));
    }

}

package ru.t1.volkova.tm.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.DescriptionEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.NameEmptyException;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProjectServiceTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    private static String USER_ID;

    @NotNull
    private static final List<ProjectDTO> projectList = new ArrayList<>();

    @NotNull
    @Getter
    private static IProjectDTOService projectService;

    @NotNull
    @Getter
    private static IUserDTOService userService;

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        projectService = context.getBean(IProjectDTOService.class);
        userService = context.getBean(IUserDTOService.class);

        @NotNull final UserDTO user = userService.create("user", "user", "user@mail.ru");
        USER_ID = user.getId();
        createProjects();
    }

    private static void createProjects() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @Nullable ProjectDTO project = projectService.create(USER_ID, "Project" + i, "Description" + i);
            projectList.add(project);
        }
    }

    @Test
    public void testCreate(
    ) {
        @Nullable final ProjectDTO project = projectService.create(USER_ID, "new_project", "new_description");
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(USER_ID, project.getId()));
    }

    @Test
    public void testCreateNegative(
    ) {
        @Nullable final ProjectDTO project = projectService.create(null, "new_project", "new description");
        Assert.assertNull(project);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) {
        projectService.create(USER_ID, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) {
        projectService.create(USER_ID, "new", null);
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @Nullable final ProjectDTO project = projectList.get(0);
        if (project == null) return;
        projectService.updateById(project.getUserId(), project.getId(), newName, newDescription);
        @Nullable final ProjectDTO newProject = projectService.findOneById(project.getUserId(), project.getId());
        if (newProject == null) return;
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateNotFoundProject(
    ) {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final String id = "non-existent-id";
        projectService.updateById(USER_ID, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        projectService.updateById(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) {
        projectService.updateById(USER_ID, projectList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) {
        projectService.updateById(USER_ID, projectList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        @Nullable final ProjectDTO project = projectService.updateByIndex(USER_ID, index, newName, newDescription);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID, 10, "name", "new description");
        projectService.updateByIndex(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID, 2, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        projectService.updateByIndex(USER_ID, index, "name", null);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final ProjectDTO projectToUpdate = projectList.get(0);
        @Nullable final ProjectDTO project = projectService.changeProjectStatusById(projectToUpdate.getUserId(), projectToUpdate.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() {
        projectService.changeProjectStatusById(USER_ID, null, Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "", Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundById() {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() {
        @Nullable final ProjectDTO project = projectService.changeProjectStatusByIndex(USER_ID, 2, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() {
        projectService.changeProjectStatusByIndex(USER_ID, 22, Status.IN_PROGRESS);
        projectService.changeProjectStatusByIndex(USER_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundByIndex() {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        Assert.assertNotNull(projectService.findOneById(USER_ID, projectList.get(index).getId()));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFindOneByIdNegative() {
        Assert.assertNotNull(projectService.findOneById(USER_ID, "non-existent"));
    }

}

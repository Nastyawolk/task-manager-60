package ru.t1.volkova.tm.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("user");
    }

}

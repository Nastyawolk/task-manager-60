package ru.t1.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IAbstractDTORepository<E extends AbstractModelDTO> {

    void add(@NotNull E entity);

    void update(@NotNull E entity);

    @NotNull
    List<E> findAll();

    @Nullable List<E> findAll(@NotNull Comparator comparator);

    void clear();

    @Nullable
    E findOneById(@NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull Integer index);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    int getSize();

//    @NotNull
//    EntityManager getEntityManager();

}

package ru.t1.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.dto.model.SessionDTO;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    @Override
    public @Nullable List<SessionDTO> findAll(@Nullable final String userId) {
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<SessionDTO> findAll(
            @Nullable final String userId,
            @NotNull final Comparator comparator
    ) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId " +
                        "ORDER BY e." + param, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId " +
                        "AND e.id = :id", SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public SessionDTO findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList().stream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        entityManager.createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId " +
                        "AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final SessionDTO session = findOneByIndex(userId, index);
        removeOneById(userId, session.getId());
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @NotNull List<SessionDTO> findAll() {
        return entityManager.createQuery("FROM SessionDTO", SessionDTO.class).getResultList();
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId " +
                        "ORDER BY e." + param, SessionDTO.class)
                .getResultList();
    }

    @Override
    @Transactional
    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO").executeUpdate();
    }

    @Override
    public @NotNull SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public @Nullable SessionDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager
                .createQuery("FROM SessionDTO", SessionDTO.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM SessionDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final SessionDTO session = findOneByIndex(index);
        if (session == null) {
            return;
        }
        removeOneById(session.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDTO e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}

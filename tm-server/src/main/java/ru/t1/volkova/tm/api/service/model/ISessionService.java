package ru.t1.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Session;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface ISessionService {

    void add(@NotNull Session entity);

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    Session findOneByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    List<Session> findAll(@Nullable String userId) throws SQLException;

    @NotNull
    List<Session> findAll(@Nullable String userId, @NotNull Comparator comparator);

    void removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

}

package ru.t1.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO>
        implements IProjectDTORepository {

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable final String userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(
            @Nullable final String userId,
            @NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId " +
                        "ORDER BY e." + param, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId " +
                        "AND e.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Override
    public @Nullable ProjectDTO findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList().stream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId " +
                        "AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        removeOneById(userId, project.getId());
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        return entityManager.createQuery("FROM ProjectDTO", ProjectDTO.class).getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId " +
                        "ORDER BY e." + param, ProjectDTO.class)
                .getResultList();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    @NotNull
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public @Nullable ProjectDTO findOneByIndex(
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("FROM ProjectDTO", ProjectDTO.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final ProjectDTO project = findOneByIndex(index);
        if (project == null) {
            return;
        }
        removeOneById(project.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}

package ru.t1.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.model.IProjectRepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IProjectService {

    @NotNull IProjectRepository getRepository();

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @Nullable
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    void clear(@Nullable String userId);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId, @NotNull Comparator comparator);

    int getSize(@Nullable String userId);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index);

}

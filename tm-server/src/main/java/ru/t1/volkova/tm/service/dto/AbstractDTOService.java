package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.volkova.tm.api.service.dto.IAbstractDTOService;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

import java.util.Comparator;
import java.util.List;

@Getter
@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IAbstractDTORepository<M>> implements IAbstractDTOService<M> {

    @NotNull
    @Autowired
    protected R repository;

    @Override
    @Transactional
    public void add(@NotNull final M entity) {
        repository.add(entity);
    }

    @Override
    @Transactional
    public void update(@NotNull final M entity) {
        repository.update(entity);
    }

    @Override
    public @NotNull List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable List<M> findAll(@NotNull final Comparator comparator) {
        return repository.findAll(comparator);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    public @Nullable M findOneById(@NotNull final String id) {
        return repository.findOneById(id);
    }

    @Override
    public @Nullable M findOneByIndex(@NotNull final Integer index) {
        return repository.findOneByIndex(index);
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        repository.removeOneById(id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        repository.removeOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}

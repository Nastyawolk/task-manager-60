package ru.t1.volkova.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;


    @Override
    @Transactional
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final ITaskDTORepository repository = taskService.getRepository();
        @NotNull final TaskDTO task = taskService.findOneById(userId, taskId);
        task.setProjectId(projectId);
        repository.update(task);
        return task;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @NotNull final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null || tasks.size() == 0) throw new TaskNotFoundException();
        for (@NotNull final TaskDTO task : tasks) {
            taskService.removeOneById(userId, task.getId());
        }
        projectService.removeOneById(userId, projectId);
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final TaskDTO task = taskService.findOneById(userId, taskId);
        @NotNull final ITaskDTORepository repository = taskService.getRepository();
        task.setProjectId(null);
        taskService.getRepository().update(task);
        return task;
    }

}

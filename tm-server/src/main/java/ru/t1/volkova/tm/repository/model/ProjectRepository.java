package ru.t1.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.model.IProjectRepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project>
        implements IProjectRepository {

    @Override
    public @Nullable List<Project> findAll(@Nullable final String userId) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<Project> findAll(
            @Nullable final String userId,
            @NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId " +
                        "ORDER BY e." + param, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Project findOneById(
            @Nullable final String userId,
            @Nullable final String id) {
        return entityManager
                .createQuery("SELECT e FROM Project e WHERE e.user.id = :userId " +
                        "AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Override
    public @Nullable Project findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList().stream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Project e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) {
            return;
        }
        removeOneById(userId, project.getId());
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        @Nullable final List<Project> projects = findAll(userId);
        if (projects == null) return;
        for (@NotNull final Project project : projects) {
            removeOneById(userId, project.getId());
        }
    }

    @Override
    public @NotNull List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @Override
    public @Nullable List<Project> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId " +
                        "ORDER BY e." + param, Project.class)
                .getResultList();
    }

    @Override
    public void clear() {
        @NotNull final List<Project> projects = findAll();
        for (@NotNull final Project project : projects) {
            removeOneById(project.getId());
        }
    }

    @Override
    @NotNull
    public Project findOneById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public @Nullable Project findOneByIndex(
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("FROM Project", Project.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(index);
        if (project == null) {
            return;
        }
        removeOneById(project.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Project e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}

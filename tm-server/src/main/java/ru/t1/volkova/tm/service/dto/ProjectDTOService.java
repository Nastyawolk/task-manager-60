package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO, IProjectDTORepository>
        implements IProjectDTOService {

    @NotNull
    @Getter
    @Autowired
    public IProjectDTORepository repository;

    @Nullable
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        if (status != null) project.setStatus(status);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneByIndex(userId, index);
        if (status != null) project.setStatus(status);
        repository.update(project);
        return project;
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        findOneById(userId, id);
        repository.removeOneById(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        findOneByIndex(userId, index);
        repository.removeOneByIndex(userId, index);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDTO project = repository.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project = repository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @SuppressWarnings("unchecked")
    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId,
                                    @NotNull final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<ProjectDTO> projects = repository.findAll(userId, comparator);
        if (projects == null) throw new ProjectNotFoundException();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<ProjectDTO> projects = repository.findAll(userId);
        if (projects == null) throw new ProjectNotFoundException();
        return projects;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<ProjectDTO> projects = repository.findAll(userId);
        if (projects == null) throw new ProjectNotFoundException();
        return repository.getSize(userId);
    }

}

package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;

@Getter
@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedDTOModel, R extends IAbstractUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    @Override
    @Transactional
    public void add(@NotNull final M entity) {
        if (entity.getUserId().isEmpty()) throw new UserIdEmptyException();
        super.add(entity);
    }

    @Override
    @Transactional
    public void update(@NotNull final M entity) {
        if (entity.getUserId().isEmpty()) throw new UserIdEmptyException();
        super.update(entity);
    }

}

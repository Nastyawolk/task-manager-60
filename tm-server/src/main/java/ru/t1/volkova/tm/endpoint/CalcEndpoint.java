package ru.t1.volkova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Controller;
import ru.t1.volkova.tm.api.endpoint.ICalcEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.ICalcEndpoint")
public final class CalcEndpoint extends AbstractEndPoint implements ICalcEndpoint {

    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") final int a,
            @WebParam(name = "b") final int b
    ) {
        return a + b;
    }

}

package ru.t1.volkova.tm.repository.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    @Override
    @Transactional
    public void add(@NotNull final M entity) {
        super.add(entity);
    }

    @Override
    @Transactional
    public void update(@NotNull final M entity) {
        super.update(entity);
    }

}

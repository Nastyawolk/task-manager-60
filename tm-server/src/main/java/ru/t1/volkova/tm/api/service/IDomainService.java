package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64(@NotNull String fileName);

    void saveDataBase64(@NotNull String fileName);

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void saveDataJsonFasterXml();

    void loadDataJsonJaxB();

    void saveDataJsonJaxB();

    void loadDataXmlFasterXml();

    void saveDataXmlFasterXml();

    void loadDataXmlJaxB();

    void saveDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

}

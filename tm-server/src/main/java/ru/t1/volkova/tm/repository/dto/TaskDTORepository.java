package ru.t1.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO>
        implements ITaskDTORepository {

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable final String userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(
            @Nullable final String userId,
            @NotNull final Comparator comparator
    ) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId " +
                        "ORDER BY e." + param, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId " +
                        "AND e.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public TaskDTO findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList().stream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId " +
                        "AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        removeOneById(userId, task.getId());
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @NotNull final String projectId
    ) {
        if (userId == null) return null;
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDTO> findAll() {
        return entityManager.createQuery("FROM TaskDTO", TaskDTO.class).getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId " +
                        "ORDER BY e." + param, TaskDTO.class)
                .getResultList();
    }

    @Override
    @Transactional
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    public @NotNull TaskDTO findOneById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public @Nullable TaskDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager
                .createQuery("FROM TaskDTO", TaskDTO.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final TaskDTO task = findOneByIndex(index);
        if (task == null) {
            return;
        }
        removeOneById(task.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}

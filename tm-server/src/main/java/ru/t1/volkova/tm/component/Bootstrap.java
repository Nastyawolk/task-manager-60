package ru.t1.volkova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.api.service.dto.*;
import ru.t1.volkova.tm.endpoint.*;
import ru.t1.volkova.tm.service.*;
import ru.t1.volkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Getter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private ISessionDTORepository sessionRepository;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    @Autowired
    private AbstractEndPoint[] endPoints;

    private void initEndPoints(@NotNull final AbstractEndPoint[] endPoints) {
        for (@NotNull final AbstractEndPoint endPoint : endPoints) {
            @NotNull final String host = propertyService.getServerHost();
            @NotNull final String port = propertyService.getServerPort().toString();
            @NotNull final String name = endPoint.getClass().getSimpleName();
            @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
            Endpoint.publish(url, endPoint);
            System.out.println(url);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        initEndPoints(endPoints);
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}

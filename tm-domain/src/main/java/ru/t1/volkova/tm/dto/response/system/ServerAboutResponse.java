package ru.t1.volkova.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.t1.volkova.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;

    private String hostName;

}

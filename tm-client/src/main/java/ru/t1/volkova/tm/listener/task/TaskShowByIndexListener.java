package ru.t1.volkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.task.TaskGetByIndexRequest;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class TaskShowByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Show task by index.";

    @NotNull
    private static final String NAME = "task-show-by-index";

    @Override
    @EventListener(condition = "@taskShowByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final TaskDTO task = getTaskEndpoint().getTaskByIndex(request).getTask();
        showTask(task);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}

package ru.t1.volkova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserRegistryRequest;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    private static final String DESCRIPTION = "Registry user";

    @NotNull
    private static final String NAME = "user-registry";

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final UserDTO user = getUserEndpoint().registryUser(request).getUser();
        showUser(user);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}

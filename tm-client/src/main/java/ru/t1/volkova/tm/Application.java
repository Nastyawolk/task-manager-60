package ru.t1.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.volkova.tm.component.Bootstrap;
import ru.t1.volkova.tm.configuration.ClientConfiguration;

public final class Application {

    public static void main(@Nullable String[] args) throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start(args);
    }

}
